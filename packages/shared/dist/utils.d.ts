import { CurvePoint, CurvePoints, Point, PercentagePoint, Dimensions, Socket } from './types';
export declare const createCurveData: (points: CurvePoints) => string | null;
export declare const toCurvePoint: (point: Point) => CurvePoint;
export declare const fromCurvePoint: (point: CurvePoint) => Point;
export declare const pointToPercentage: (point: Point, dimensions: Dimensions) => PercentagePoint;
export declare const percentageToPoint: (point: PercentagePoint, dimensions: Dimensions) => Point;
export declare const sendSocketMessage: (socket: Socket, payload: unknown) => void;
