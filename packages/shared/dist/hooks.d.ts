import { DrawingState, Socket, handleDrawingFunc, DrawingDeps } from './types';
export declare const useSocketConfiguration: (url: string) => WebSocket;
export declare const useStartDrawing: (socket: Socket) => () => void;
export declare const useFinishDrawing: (socket: Socket) => () => void;
export declare const useClearArea: (socket: Socket) => () => void;
export declare const useDrawing: ({ socket, dimensions }: DrawingDeps) => handleDrawingFunc;
export declare const useDrawingLib: ({ dimensions, socket }: DrawingDeps) => DrawingState;
