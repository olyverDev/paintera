"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useDrawingLib = exports.useDrawing = exports.useClearArea = exports.useFinishDrawing = exports.useStartDrawing = exports.useSocketConfiguration = void 0;
const react_1 = require("react");
const constants_1 = require("./constants");
const utils_1 = require("./utils");
const useSocketConfiguration = (url) => {
    const socket = react_1.useMemo(() => new WebSocket(url), [url]);
    react_1.useEffect(() => {
        return () => { socket === null || socket === void 0 ? void 0 : socket.close(); };
    }, []);
    return socket;
};
exports.useSocketConfiguration = useSocketConfiguration;
const useStartDrawing = (socket) => react_1.useCallback(() => {
    utils_1.sendSocketMessage(socket, { type: constants_1.COMMUNICATION_EVENT_TYPES.startDrawing });
}, []);
exports.useStartDrawing = useStartDrawing;
const useFinishDrawing = (socket) => react_1.useCallback(() => {
    utils_1.sendSocketMessage(socket, { type: constants_1.COMMUNICATION_EVENT_TYPES.finishDrawing });
}, [socket]);
exports.useFinishDrawing = useFinishDrawing;
const useClearArea = (socket) => react_1.useCallback(() => {
    utils_1.sendSocketMessage(socket, { type: constants_1.COMMUNICATION_EVENT_TYPES.clearArea });
}, [socket]);
exports.useClearArea = useClearArea;
const useDrawing = ({ socket, dimensions }) => react_1.useCallback((point) => {
    utils_1.sendSocketMessage(socket, {
        type: constants_1.COMMUNICATION_EVENT_TYPES.sendPoint,
        payload: utils_1.pointToPercentage(point, dimensions),
    });
}, [socket, dimensions]);
exports.useDrawing = useDrawing;
const useDrawingLib = ({ dimensions, socket }) => {
    const [curves, setCurves] = react_1.useState([]);
    const [drawing, setDrawing] = react_1.useState(false);
    react_1.useEffect(() => {
        socket.onmessage = (event) => {
            const data = JSON.parse(event.data);
            switch (data.type) {
                case constants_1.COMMUNICATION_EVENT_TYPES.startDrawing: {
                    setDrawing(true);
                    break;
                }
                case constants_1.COMMUNICATION_EVENT_TYPES.finishDrawing: {
                    setDrawing(false);
                    setCurves([...curves, []]);
                    break;
                }
                case constants_1.COMMUNICATION_EVENT_TYPES.sendPoint: {
                    const curvesCopy = [...curves];
                    if (!curvesCopy[curvesCopy.length - 1]) {
                        curvesCopy.push([]);
                    }
                    const point = utils_1.percentageToPoint(data.payload, dimensions);
                    curvesCopy[curvesCopy.length - 1].push(utils_1.toCurvePoint(point));
                    setCurves(curvesCopy);
                    break;
                }
                case constants_1.COMMUNICATION_EVENT_TYPES.clearArea: {
                    setCurves([]);
                    break;
                }
                default:
                    break;
            }
        };
    }, [socket, dimensions, curves]);
    return {
        drawing,
        curves: curves.map(utils_1.createCurveData),
        handleStart: exports.useStartDrawing(socket),
        handleDrawing: exports.useDrawing({ socket, dimensions }),
        handleFinish: exports.useFinishDrawing(socket),
        handleClearArea: exports.useClearArea(socket),
    };
};
exports.useDrawingLib = useDrawingLib;
