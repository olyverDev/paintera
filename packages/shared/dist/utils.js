"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendSocketMessage = exports.percentageToPoint = exports.pointToPercentage = exports.fromCurvePoint = exports.toCurvePoint = exports.createCurveData = void 0;
const d3_shape_1 = require("d3-shape");
const createCurveData = (points) => d3_shape_1.line().curve(d3_shape_1.curveCatmullRom)(points);
exports.createCurveData = createCurveData;
const toCurvePoint = (point) => [point.x, point.y];
exports.toCurvePoint = toCurvePoint;
const fromCurvePoint = (point) => {
    const [x, y] = point;
    return { x, y };
};
exports.fromCurvePoint = fromCurvePoint;
const pointToPercentage = (point, dimensions) => ({ x: point.x / dimensions.width, y: point.y / dimensions.height });
exports.pointToPercentage = pointToPercentage;
const percentageToPoint = (point, dimensions) => ({ x: point.x * dimensions.width, y: point.y * dimensions.height });
exports.percentageToPoint = percentageToPoint;
const sendSocketMessage = (socket, payload) => {
    if ((socket === null || socket === void 0 ? void 0 : socket.readyState) === WebSocket.OPEN) {
        socket.send(JSON.stringify(payload));
    }
};
exports.sendSocketMessage = sendSocketMessage;
