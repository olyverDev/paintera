"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.COMMUNICATION_EVENT_TYPES = void 0;
exports.COMMUNICATION_EVENT_TYPES = {
    startDrawing: 'startDrawing',
    finishDrawing: 'finishDrawing',
    sendPoint: 'sendPoint',
    clearArea: 'clearArea',
};
