export declare const COMMUNICATION_EVENT_TYPES: {
    startDrawing: string;
    finishDrawing: string;
    sendPoint: string;
    clearArea: string;
};
