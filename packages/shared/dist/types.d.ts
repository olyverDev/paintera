export declare type CurvePoint = [number, number];
export declare type CurvePoints = CurvePoint[];
export declare type CurveParts = CurvePoints[];
export declare type Point = {
    x: number;
    y: number;
};
export declare type Points = Point[];
export declare type PercentagePoint = Point;
export declare type CommunicationEvent = {
    type: string;
    payload?: PercentagePoint | null;
};
export declare type Dimensions = {
    width: number;
    height: number;
};
export declare type SvgPathDataAttributes = string[];
export declare type handleDrawingFunc = (point: Point) => void;
export declare type DrawingState = {
    drawing: boolean;
    curves: SvgPathDataAttributes;
    handleStart: () => void;
    handleFinish: () => void;
    handleDrawing: handleDrawingFunc;
    handleClearArea: () => void;
};
export declare type Socket = WebSocket | null;
export interface DrawingDeps {
    socket: Socket;
    dimensions: Dimensions;
}
