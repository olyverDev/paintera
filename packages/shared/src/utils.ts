import { line as d3Line, curveCatmullRom } from 'd3-shape';
import { COMMUNICATION_EVENT_TYPES } from './constants';
import {
  CurvePoint,
  CurvePoints,
  Point,
  PercentagePoint,
  Dimensions,
  Socket,
} from './types';

export const createCurveData = (points: CurvePoints): string | null =>
  d3Line().curve(curveCatmullRom)(points);

export const toCurvePoint = (point: Point): CurvePoint => [point.x, point.y];
export const fromCurvePoint = (point: CurvePoint): Point => {
  const [x, y] = point;
  return { x, y };
};

export const pointToPercentage = (point: Point, dimensions: Dimensions): PercentagePoint => (
  { x: point.x / dimensions.width, y: point.y / dimensions.height }
);

export const percentageToPoint = (point: PercentagePoint, dimensions: Dimensions): Point => (
  { x: point.x * dimensions.width, y: point.y * dimensions.height }
);

export const sendSocketMessage = (socket: Socket, payload: unknown): void => {
  if (socket?.readyState === WebSocket.OPEN) {
    socket.send(JSON.stringify(payload));
  }
};
