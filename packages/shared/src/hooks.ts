import { useState, useEffect, useMemo, useCallback } from 'react';
import { COMMUNICATION_EVENT_TYPES } from './constants';
import { CurveParts, DrawingState, Socket, Point, handleDrawingFunc, DrawingDeps } from './types';
import {
  createCurveData,
  percentageToPoint,
  sendSocketMessage,
  toCurvePoint,
  pointToPercentage,
} from './utils';

export const useSocketConfiguration = (url: string): WebSocket => {
  const socket = useMemo(() => new WebSocket(url), [url]);

  useEffect(() => {
    return () => { socket?.close(); };
  }, []);

  return socket;
};

export const useStartDrawing = (socket: Socket) => useCallback(() => {
  sendSocketMessage(
    socket,
    { type: COMMUNICATION_EVENT_TYPES.startDrawing },
  );
}, []);

export const useFinishDrawing = (socket: Socket) => useCallback(() => {
  sendSocketMessage(
    socket,
    { type: COMMUNICATION_EVENT_TYPES.finishDrawing },
  );
}, [socket]);

export const useClearArea = (socket: Socket) => useCallback(() => {
  sendSocketMessage(
    socket,
    { type: COMMUNICATION_EVENT_TYPES.clearArea },
  );
}, [socket]);

export const useDrawing = ({ socket, dimensions }: DrawingDeps): handleDrawingFunc => useCallback((point: Point): void => {
  sendSocketMessage(
    socket,
    {
      type: COMMUNICATION_EVENT_TYPES.sendPoint,
      payload: pointToPercentage(point, dimensions),
    },
  );
}, [socket, dimensions]);

export const useDrawingLib = ({ dimensions, socket }: DrawingDeps): DrawingState => {
  const [curves, setCurves] = useState<CurveParts>([]);
  const [drawing, setDrawing] = useState<boolean>(false);

  useEffect(() => {
    socket.onmessage = (event) => {
      const data = JSON.parse(event.data);

      switch (data.type) {
        case COMMUNICATION_EVENT_TYPES.startDrawing: {
          setDrawing(true);
          break;
        }
        case COMMUNICATION_EVENT_TYPES.finishDrawing: {
          setDrawing(false);
          setCurves([...curves, []]);
          break;
        }
        case COMMUNICATION_EVENT_TYPES.sendPoint: {
          const curvesCopy = [...curves];

          if (!curvesCopy[curvesCopy.length - 1]) {
            curvesCopy.push([]);
          }

          const point = percentageToPoint(data.payload, dimensions);
          curvesCopy[curvesCopy.length - 1].push(toCurvePoint(point));
          setCurves(curvesCopy);
          break;
        }
        case COMMUNICATION_EVENT_TYPES.clearArea: {
          setCurves([]);
          break;
        }
        default:
          break;
      }
    };
  }, [socket, dimensions, curves]);

  return {
    drawing,
    curves: curves.map(createCurveData),
    handleStart: useStartDrawing(socket),
    handleDrawing: useDrawing({ socket, dimensions }),
    handleFinish: useFinishDrawing(socket),
    handleClearArea: useClearArea(socket),
  };
};
