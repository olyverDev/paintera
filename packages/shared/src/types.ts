export type CurvePoint = [number, number]; // x, y
export type CurvePoints = CurvePoint[];
export type CurveParts = CurvePoints[];

export type Point = { x: number; y: number };
export type Points = Point[];
export type PercentagePoint = Point;

export type CommunicationEvent = {
  type: string;
  payload?: PercentagePoint | null;
};

export type Dimensions = {
  width: number;
  height: number;
};

export type SvgPathDataAttributes = string[];

export type handleDrawingFunc = (point: Point) => void;

export type DrawingState = {
  drawing: boolean;
  curves: SvgPathDataAttributes;
  handleStart: () => void;
  handleFinish: () => void;
  handleDrawing: handleDrawingFunc;
  handleClearArea: () => void;
};

export type Socket = WebSocket | null;

export interface DrawingDeps {
  socket: Socket;
  dimensions: Dimensions;
}
