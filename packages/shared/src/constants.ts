export const COMMUNICATION_EVENT_TYPES = {
  startDrawing: 'startDrawing',
  finishDrawing: 'finishDrawing',
  sendPoint: 'sendPoint',
  clearArea: 'clearArea',
};
