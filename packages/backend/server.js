const WSLib = require('ws');
const express = require('express');
const http = require('http');

require('dotenv').config()

const server = http.createServer(express);
const WebSocketServer = new WSLib.Server({ server })

WebSocketServer.on('connection', (socket) => {
  socket.on('message', (data) => {
    WebSocketServer.clients.forEach((client) => {
      if (client.readyState === WSLib.OPEN) {
        client.send(data);
      }
    });
  });

  console.log('connection..');
});

server.listen(process.env.PORT, () => {
  console.log(`Server is listening on ${process.env.PORT} port.`)
});
