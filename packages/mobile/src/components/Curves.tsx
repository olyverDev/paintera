import React from 'react';
import { SvgPathDataAttributes } from 'paintera-shared';
import { Path } from 'react-native-svg';

export interface CurvesProps {
  curves: SvgPathDataAttributes;
}

const Curves: React.FC<CurvesProps> = ({ curves }) => (
  <>
    {curves.map(
      (curveD, idx) =>
        curveD && (
          <Path key={idx} d={curveD} fill="none" stroke="black" />
        ),
    )}
  </>
);

export default Curves;
