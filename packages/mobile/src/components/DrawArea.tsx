import React, { useCallback } from 'react';
import { View, useWindowDimensions } from 'react-native';
import Svg from 'react-native-svg';
import { useSocketConfiguration, useDrawingLib } from 'paintera-shared';

import env from '../../env';
import Curves from './Curves';

const DrawArea: React.FC<{}> = () => {
  const socket = useSocketConfiguration(env.wsPath);
  const dimensions = useWindowDimensions();
  const {
    drawing,
    curves,
    handleStart,
    handleDrawing,
    handleFinish,
  } = useDrawingLib({ dimensions, socket });
  const handleTouchMove = useCallback((event: MouseEvent) => {
    event.persist();
    handleDrawing({
      x: event.nativeEvent.locationX,
      y: event.nativeEvent.locationY,
    });
  }, [handleDrawing]);

  return (
    <View>
      <Svg
        width="100%"
        height="100%"
        onTouchStart={handleStart}
        onTouchMove={drawing ? handleTouchMove : undefined}
        onTouchEnd={handleFinish}
      >
        <Curves curves={curves} />
      </Svg>
    </View>
  );
};

export default DrawArea;
