import React from 'react';
import './App.css';

import DrawArea from './components/DrawArea';

function App() {
  return (
    <div className="App">
      <DrawArea />
    </div>
  );
}

export default App;
