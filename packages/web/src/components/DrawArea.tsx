import React, { useState, MouseEvent, useCallback } from 'react'
import Measure from 'react-measure';
import {
  Dimensions,
  useSocketConfiguration,
  useDrawingLib,
} from 'paintera-shared';
import Curves from './Curves';

const DrawArea: React.FC<{}> = () => {
  const socket = useSocketConfiguration(process.env.REACT_APP_WS_PATH || '');
  const [dimensions, setDimensions] = useState<Dimensions>({ width: 0, height: 0 });
  const {
    drawing,
    curves,
    handleStart,
    handleDrawing,
    handleFinish,
    handleClearArea,
  } = useDrawingLib({ dimensions, socket });

  const handleMouseMove = useCallback((event: MouseEvent) => {
    event.persist();
    handleDrawing({ x: event.clientX, y: event.clientY });
  }, [handleDrawing]);

  return (
    <Measure
      bounds
      onResize={contentRect => {
        setDimensions(prev => contentRect.bounds || prev)
      }}
    >
      {({ measureRef }) => (
        <svg
          ref={measureRef}
          width="100%"
          height="100%"
          onMouseDown={handleStart}
          onMouseUp={handleFinish}
          onMouseMove={drawing ? handleMouseMove : undefined}
          onDoubleClick={handleClearArea}
        >
          <Curves curves={curves} />
        </svg>
      )}
    </Measure>
  );
}

export default DrawArea;